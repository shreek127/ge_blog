package com.aem.ge.blog.core.services.impl;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.ge.blog.core.services.BlogListService;

@Component(immediate = true, service = BlogListService.class)
public class BlogListServiceImpl implements BlogListService {

	private final Logger LOGGER = LoggerFactory.getLogger(BlogListServiceImpl.class);
	
	
}
