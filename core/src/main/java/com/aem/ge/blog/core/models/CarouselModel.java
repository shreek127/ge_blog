package com.aem.ge.blog.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import com.aem.ge.blog.core.models.datamodels.CarouselDataModel;

@Model(adaptables = Resource.class, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class CarouselModel {

	@ChildResource
	private List<CarouselDataModel> carouselItems;

	public List<CarouselDataModel> getCarouselItems() {
		return carouselItems;
	}
}
