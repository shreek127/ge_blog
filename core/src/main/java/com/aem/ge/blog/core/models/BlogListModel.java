package com.aem.ge.blog.core.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.ge.blog.core.services.BlogListService;
import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Model(adaptables = Resource.class, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class BlogListModel {
	
	private final Logger LOGGER = LoggerFactory.getLogger(BlogListModel.class);
	private final String REL_PATH = "root/responsivegrid/blogarticle";
	
	@ValueMapValue
	private String heading;
	
	@ValueMapValue
	private String rootPath;
	
	@ValueMapValue
	private String buttonLabel;
	
	@ValueMapValue
	private String[] articleTags;
	
	@OSGiService
	private BlogListService service;
	
	@SlingObject
	private ResourceResolver resourceResolver;
	
	private Boolean isLoadMore = false;
	
	public List<BlogArticleModel> getArticleList() {
		LOGGER.info(" Method : getArticleList() : Starts");
		TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<BlogArticleModel> articles = new ArrayList<BlogArticleModel>();

		RangeIterator<Resource> resourceRangeIterator = tagManager.find(rootPath, articleTags, true);
		while (resourceRangeIterator.hasNext()) {
			Resource resultResource = resourceRangeIterator.next();
			LOGGER.debug(" Resource found : {} ", resultResource.getPath());
			Resource articleResource = resultResource.getChild(REL_PATH);
			if (null == articleResource) {
				LOGGER.error(" Method : getArticleList() : Page does not have article component : {} ", resultResource.getPath());
				continue;
			}
			Page page = pageManager.getContainingPage(resultResource);
			
			BlogArticleModel articleModel = articleResource.adaptTo(BlogArticleModel.class);
			articleModel.setPagePath(page.getPath());
			
			articles.add(articleModel);
		}
		if (articles.size() > 6) {
			isLoadMore = true;
		}
		LOGGER.info(" Method : getArticleList() : Ends"); 
		
		return articles.stream().sorted((a1, a2) -> a2.getPubDate().compareTo(a1.getPubDate()))
			.collect(Collectors.toList());
		
	}

	public String getHeading() {
		return heading;
	}

	public String getButtonLabel() {
		return buttonLabel;
	}

	public Boolean getIsLoadMore() {
		return isLoadMore;
	}

}
