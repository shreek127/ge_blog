package com.aem.ge.blog.core.models.datamodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.aem.ge.blog.core.utils.PathUtil;

@Model(adaptables = Resource.class, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class CarouselDataModel {
	
	@ValueMapValue
	private String heading;
	
	@ValueMapValue
	private String description;
	
	@ValueMapValue
	private String badge;
	
	@ValueMapValue
	private String image;
	
	@ValueMapValue
	private String alttext;
	
	@ValueMapValue
	private String ctaLabel;
	
	@ValueMapValue
	private String ctaLink;
	
	@ValueMapValue
	private String isNewTab;

	public String getHeading() {
		return heading;
	}

	public String getDescription() {
		return description;
	}

	public String getBadge() {
		return badge;
	}

	public String getImage() {
		return image;
	}

	public String getAlttext() {
		return alttext;
	}

	public String getCtaLabel() {
		return ctaLabel;
	}

	public String getCtaLink() {
		return PathUtil.getValidLink(ctaLink);
	}

	public String getIsNewTab() {
		return isNewTab;
	}

}
