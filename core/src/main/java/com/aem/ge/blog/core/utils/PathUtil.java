package com.aem.ge.blog.core.utils;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PathUtil {

	private PathUtil() {
		// implicit constructor
	}

	private static final Logger LOG = LoggerFactory.getLogger(PathUtil.class);
	private static final String HTML = ".html";
	
	/**
	 * This method return the internal link with .html extension
	 * 
	 * @param linkUrl string link
	 * @return validlink
	 */
	public static String getValidLink(String linkUrl) {
		if (StringUtils.isBlank(linkUrl)) {
			return StringUtils.EMPTY;
		}
		Boolean status = true;
		if (linkUrl.toLowerCase().startsWith("http://") || linkUrl.toLowerCase().startsWith("https://")
				|| linkUrl.toLowerCase().startsWith("//")) {
			status = false;
		}
		if (Boolean.TRUE.equals(status) && !linkUrl.contains(HTML) && !linkUrl.contains("/content/dam")) {
			try {
				if (linkUrl.contains("#")) {
					linkUrl = getString(linkUrl, "#", "#");
				} else if (linkUrl.contains("?")) {
					linkUrl = URIUtil.encodeQuery(linkUrl);
					linkUrl = getString(linkUrl, "\\?", "?");
				} else {
					linkUrl = StringUtils.join(linkUrl, HTML);
				}
				LOG.debug("Link is {}", linkUrl);
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error("[Exception][PathUtils] linkurl too short", e);
			} catch (URIException ex) {
				LOG.error("[Exception][PathUtils] Exception in encoding of query parameters.", ex);
			}
		}
		return linkUrl;
	}

	private static String getString(String linkUrl, String s, String s2) {
		String[] link = linkUrl.split(s, 0);
		if (link.length != 0) {
			if (link.length >= 2) {
				linkUrl = StringUtils.join(link[0], HTML, s2, link[1]);
			} else {
				linkUrl = StringUtils.join(link[0], HTML, s2);
			}
		}
		return linkUrl;
	}
}
