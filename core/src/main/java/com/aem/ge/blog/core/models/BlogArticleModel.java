package com.aem.ge.blog.core.models;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.aem.ge.blog.core.utils.PathUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class BlogArticleModel {
	
	@ValueMapValue
	private String heading;
	
	@ValueMapValue
	private String description;
	
	@ValueMapValue
	private Calendar pubDate;
	
	@ValueMapValue
	private String pubText;
	
	@ValueMapValue
	private String authorlabel;
	
	@ValueMapValue
	private String author;
	
	@ValueMapValue
	private String fileReference;
	
	@ValueMapValue
	private String badge;
	
	@ScriptVariable
	private Page currentPage;
	
	private String pagePath;

	public Tag[] getTags() {
		return currentPage.getTags();
	}
	
	public String getPagePath() {
		if (currentPage != null) {
			return PathUtil.getValidLink(currentPage.getPath());
		}
		return PathUtil.getValidLink(pagePath);
	}
	
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

	public String getPubText() {
		String date = new SimpleDateFormat("dd/MM/yyyy").format(pubDate.getTime());
		long days = ChronoUnit.DAYS.between(currentPage.getLastModified().toInstant(), Calendar.getInstance().toInstant());
		String daysBetween = StringUtils.EMPTY;
		if (days == 0) {
			daysBetween = "today";
		} else if (days == 1) {
			daysBetween = "yesterday";
		} else {
			daysBetween = days + " days ago";
		}
		return MessageFormat.format(pubText, date, daysBetween);
	}

	public String getHeading() {
		return heading;
	}

	public String getDescription() {
		return description;
	}

	public Calendar getPubDate() {
		return pubDate;
	}

	public String getAuthorlabel() {
		return authorlabel;
	}

	public String getAuthor() {
		return author;
	}

	public String getFileReference() {
		return fileReference;
	}

	public String getBadge() {
		return badge;
	}
}
