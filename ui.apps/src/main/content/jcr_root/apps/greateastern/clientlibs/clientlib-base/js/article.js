$(document).ready(function(){
  console.log( "ready!" );
  
 	$('#read-mins').text(getReadMinutes());
});

function getUrlParams(url) {
  var params = {};
  (url + '?').split('?')[1].split('&').forEach(function (pair) {
    pair = (pair + '=').split('=').map(decodeURIComponent);
    if (pair[0].length) {
      params[pair[0]] = pair[1];
    }
  });
  return params;
}

function getReadMinutes() {
	
	var count = 0;
	$('.article-content p').each(function(i, v) {
		count += $(this).text().split(' ').length;
		console.log(count);
	});
	return Math.round(count/100) + 1 ;
}